﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Exercise03
{
    public partial class Form1 : Form
    {
        private delegate void addItemDelegate(string s);
        private delegate void updateItemDelegate(string s, int index);
        private delegate void removeItemDelegate(int index);
        public Form1()
        {
            InitializeComponent();
        }

        private void BtnDownload_click(object sender, EventArgs e)
        {
            ParameterizedThreadStart p = new ParameterizedThreadStart(download);
            Thread t = new Thread(p);
            t.Start(txtUrl.Text);
        }

        private void download(object obj)
        {
            using (WebClient web = new WebClient())
            {
                string url = txtUrl.Text;
                string filename = Path.GetFileName(url);

                downloadList.Invoke(new addItemDelegate(addItem), filename);

                web.DownloadFileAsync(new Uri(url), filename);

                web.DownloadProgressChanged += UpdateProgress(filename, downloadList.Items.Count - 1);

                web.DownloadFileCompleted += DownloadComplete(downloadList.Items.Count - 1);
            }
        }

        private AsyncCompletedEventHandler DownloadComplete(int index)
        {
            Action<object, AsyncCompletedEventArgs> action = (sender, e) =>
            {
                if (e.Cancelled)
                {
                    Console.WriteLine("File download cancelled.");
                }

                if (e.Error != null)
                {
                    Console.WriteLine(e.Error.ToString());
                }


                downloadList.Invoke(new removeItemDelegate(removeItem), index);
            };
            return new AsyncCompletedEventHandler(action);
        }

        private DownloadProgressChangedEventHandler UpdateProgress(string filename, int index)
        {
            Action<object, DownloadProgressChangedEventArgs> action = (sender, e) =>
            {
                string _filename = filename + "-" + (e.ProgressPercentage).ToString() + "%";
                downloadList.Invoke(new updateItemDelegate(updateItem), _filename, index);

            };
            return new DownloadProgressChangedEventHandler(action);
        }

        private void addItem(string filename)
        {
            downloadList.Items.Add(filename);
        }

        private void removeItem(int index)
        {
            downloadList.Items.RemoveAt(index);
            //downloadList.Items.Clear();
        }

        private void updateItem(string filename, int index)
        {
            downloadList.Items[index].Text = filename;
        }
    }
}
